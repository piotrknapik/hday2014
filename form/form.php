<form class="form-horizontal" method="post" action=".">
    <fieldset>

        <!-- Form Name -->
        <h1><span class="glyphicon glyphicon-flash"></span> Quality Meerkat</h1>

        <!-- Text input-->
        <div class="control-group">
            <label class="control-label" for="input-text">Text</label>
            <div class="controls">
                <input id="input-text" name="input-text" type="text" placeholder="The-world-is-on-fire" class="input-lg">
                <p class="help-block">Please input the message for broadcasting</p>
            </div>
        </div>

        <!-- Text input-->
        <div class="control-group">
            <label class="control-label" for="input-imgurl">Image Url</label>
            <div class="controls">
                <input id="input-imgurl" name="input-imgurl" type="text" placeholder="with http://" class="input-lg">
                <p class="help-block">Please provide the url to the image... if applicable</p>
            </div>
        </div>

        <!-- Button -->
        <div class="control-group">
            <div class="controls">
                <button type="submit" id="submit-error" name="submit-btn" value="error" class="btn btn-danger"><span class="glyphicon glyphicon-fire"></span> Send as Danger</button>
                <button type="submit" id="submit-warning" name="submit-btn" value="warning" class="btn btn-warning"><span class="glyphicon glyphicon-question-sign"></span> Send as Warning</button>
                <button type="submit" id="submit-info" name="submit-btn" value="info" class="btn btn-info"><span class="glyphicon glyphicon-flag"></span> Send as Info</button>
            </div>
        </div>

    </fieldset>
</form>



