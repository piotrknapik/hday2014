<?php

require_once './lib/httpful.phar';
$uri = 'http://ec2-54-171-142-199.eu-west-1.compute.amazonaws.com/api/v1/messages';

$data['text'] = filter_input(INPUT_POST, 'input-text');
$data['imgurl'] = filter_input(INPUT_POST, 'input-imgurl');

if (!empty($_POST['input-text'])) {
    $body = 'text=' . $_POST['input-text'];
    if ($_POST['input-imgurl']) {
        $body .= '&imgurl=' . $_POST['input-imgurl'];
    }
    if (isset($_POST['submit-btn'])) {
        $body .= '&style=' . $_POST['submit-btn'];
    }
    $body .= '&type=text';
    $response = \Httpful\Request::post($uri)->body($body)->send();
}


$response = \Httpful\Request::get($uri)->send();
$messages = json_decode($response->body);
if (empty($messages)) {
    return;
}

echo '<div class="preview">';
echo '<h4>Last message preview</h4>';
//echo '<pre class="xdebug-var-dump">';
//foreach ($messages as $msg) {
    var_dump($messages[0]);
//}
//echo '</pre>';
echo '</div>';