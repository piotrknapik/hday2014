<?php

require_once dirname(__FILE__) . '/vendor/autoload.php';

use API\Application;

//Init Appliaction mode
if (empty($_ENV['SLIM_MODE'])) {
    $_ENV['SLIM_MODE'] = (getenv('SLIM_MODE'))
        ? getenv('SLIM_MODE') : 'development';
}

//Init and load configuration
$config = [];

$configFile = dirname(__FILE__) . '/config/' . $_ENV['SLIM_MODE'] . '.php';
if (is_readable($configFile)) {
    require_once $configFile;
} else {
    require_once dirname(__FILE__) . '/config/default.php';
}

//Create appliaction
$app = new Application($config['app']);

//Production logs
$app->configureMode('production', function () use ($app) {
    $app->config(array(
        'log.enable' => true,
        'log.level' => \Slim\Log::WARN,
        'debug' => false
    ));
});
//Development logs
$app->configureMode('development', function () use ($app) {
    $app->config(array(
        'log.enable' => true,
        'log.level' => \Slim\Log::DEBUG,
        'debug' => false
    ));
});

//Logger
$log = $app->getLog();

//Connect to database
try {
    $redis = new Redis();
    $redis->connect($config['db']['host'], $config['db']['port']);
    $app->setSource($redis);
} catch (\Exception $e) {
    $log->error($e->getMessage());
}



//Middlewares

//JSON
//$app->add(new API\Middleware\JSON('api/v1'));