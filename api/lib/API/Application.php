<?php

namespace API;

use Slim\Slim;

class Application extends Slim
{

    /**
     * @var \Redis
     */
    protected $redis = null;
    protected $list = '';

    public function setSource($source)
    {
        $this->redis = $source;
        $this->list = $this->container['settings']['redis_queue'];
    }

    public function insert($data)
    {
        $dateTime = new \DateTime();
        $data['timestamp'] = $dateTime->getTimestamp();
        $json = json_encode($data);
        $this->redis->rPush($this->list, $json);
        $curl = curl_init("http://ec2-54-171-142-199.eu-west-1.compute.amazonaws.com:6081/api/v1/messages");
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PURGE");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_exec($curl);

        return $data;
    }

    public function getMessages($timestamp, $limit)
    {
        if ($limit <= 0) {
            $limit = 10;
        }

        if ($timestamp !== '') {
            $timestamp = new \DateTime("@$timestamp");
            $newer = [];
            $list = $this->redis->lRange($this->list, 0, -1);
            $list = array_reverse($list);
            foreach ($list as $jsonEntry) {
                $entry = json_decode($jsonEntry);
                $date = new \DateTime("@$entry->timestamp");
                if ($date > $timestamp) {
                    $newer[] = $entry;
                } else {
                    break;
                }
            }
            return array_slice($newer, 0, $limit);
        }

        $list = $this->redis->lRange($this->list, -$limit, -1);
        $list = array_map(function ($el) {
            return json_decode($el);
        }, $list);
        $list = array_reverse($list);
        return $list;
    }
}