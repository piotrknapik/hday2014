<?php

namespace API\Middleware;

use Slim\Middleware;

class JSON extends Middleware
{

    protected $root = '';

    public function __construct($root)
    {
        $this->root = $root;
    }

    public function call()
    {
        $regex = '|^' . $this->root . '.*|';

        if (!preg_match($regex, $this->app->request->getResourceUri())) {
            $this->next->call();
        }

        $this->app->response->headers->set(
            'Content-Type',
            'application/json'
        );

        $method = strtolower($this->app->request->getMethod());
        $mediaType = $this->app->request->getMediaType();

        $availableMethods = ['post', 'put', 'patch'];

        if (in_array($method, $availableMethods) && $this->app->request()->getBody() !== '') {
            if (empty($mediaType) /*|| $mediaType !== 'application/json' */) {
                $this->app->halt(415);
            }
        }
        //$this->next->call();
    }
}