<?php

error_reporting(E_ALL);

$config['app'] = [
    'mode' => $_ENV['SLIM_MODE'],
    'redis_queue' => 'hackday112014_queue',
    'cache.ttl' => 60,
    'rate.limit' => 1000,
    'log.writer' => new \Flynsarmy\SlimMonolog\Log\MonologWriter([
        'handlers' => [
            new \Monolog\Handler\StreamHandler(
                realpath(__DIR__ . '/../logs') . '/' . $_ENV['SLIM_MODE'] . '_' . date('Y-m-d') . '.log'
            )
        ]
    ])
];

$config['db'] = [
    'host' => '127.0.0.1',
    'port' => 6379
];