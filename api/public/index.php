<?php
require_once dirname(__FILE__) . '/../bootstrap.php';

use Swagger\Annotations as SWG;

/**
 * @SWG\Resource(
 *     apiVersion="0.2",
 *     swaggerVersion="1.2",
 *     resourcePath="/pet",
 *     basePath="http://petstore.swagger.wordnik.com/api"
 * )
 */

/**
 *
 * @SWG\Api(
 *   path="/pet.{format}/{petId}",
 *   description="Operations about pets",
 *   @SWG\Operation(
 *      method="GET", summary="Find pet by ID", notes="Returns a pet based on ID",
 *     type="Pet", nickname="getPetById",
 *   @SWG\Parameter(
 *     name="petId",
 *     description="ID of pet that needs to be fetched",
 *     paramType="path",
 *     required=true,
 *     allowMultiple=false,
 *     type="string"),
 *      @SWG\ResponseMessage(code=404, message="Pet not found"),
 *      @SWG\ResponseMessage(code=404, message="Pet not found")
 *   )
 * )
 */

$app->group('/api', function () use ($app, $log) {


    $app->group('/v1', function () use ($app, $log) {

        $app->get('/messages', function () use ($app, $log) {
            $limit = (int)$app->request->get('limit');
            $timestamp = (string)($app->request->get('newer_than'));

            $messages = $app->getMessages($timestamp, $limit);

            $app->response->setBody(json_encode($messages, JSON_PRETTY_PRINT));
            return;
        });

        $app->post('/messages', function () use ($app, $log) {
            $body = $app->request()->getBody();
            parse_str($body, $data);
            $record = $app->insert($data);
            $app->response->setBody(json_encode($record, JSON_PRETTY_PRINT));
            return;
        });
    });
});

//Handle 404
$app->notFound(function () use ($app) {
    $mediaType = $app->request->getMediaType();
    $isAPI = (bool)preg_match('|^/api/v.*$|', $app->request->getPath());

    if ('application/json' === $mediaType || true === $isAPI) {
        $app->response->headers->set(
            'Content-Type',
            'application/json'
        );

        echo json_encode(
            array(
                'code' => 404,
                'message' => 'Not found'
            )
        );

    } else {
        echo '<html>
        <head><title>404 Page Not Found</title></head>
        <body><h1>404 Page Not Found</h1><p>The page you are
        looking for could not be found.</p></body></html>';
    }
});

$app->run();